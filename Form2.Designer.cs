﻿namespace BazyDanyc
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxTele = new System.Windows.Forms.TextBox();
            this.textBoxDyrektor = new System.Windows.Forms.TextBox();
            this.textBoxRemont = new System.Windows.Forms.TextBox();
            this.textBoxAdres = new System.Windows.Forms.TextBox();
            this.textBoxNR = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonDel = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.dataGridViewBank = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewIns = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewMiej = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button5 = new System.Windows.Forms.Button();
            this.buttonAddIns = new System.Windows.Forms.Button();
            this.buttonDelMiej = new System.Windows.Forms.Button();
            this.buttonAddMiej = new System.Windows.Forms.Button();
            this.textBoxSD = new System.Windows.Forms.TextBox();
            this.textBoxSS = new System.Windows.Forms.TextBox();
            this.textBoxSwiatlo = new System.Windows.Forms.TextBox();
            this.textBoxWoje = new System.Windows.Forms.TextBox();
            this.textBoxMiejscowosc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBank)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMiej)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1093, 514);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1085, 485);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Bank";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxTele);
            this.panel1.Controls.Add(this.textBoxDyrektor);
            this.panel1.Controls.Add(this.textBoxRemont);
            this.panel1.Controls.Add(this.textBoxAdres);
            this.panel1.Controls.Add(this.textBoxNR);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.buttonDel);
            this.panel1.Controls.Add(this.buttonAdd);
            this.panel1.Controls.Add(this.dataGridViewBank);
            this.panel1.Location = new System.Drawing.Point(3, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1083, 479);
            this.panel1.TabIndex = 0;
            // 
            // textBoxTele
            // 
            this.textBoxTele.Location = new System.Drawing.Point(170, 202);
            this.textBoxTele.Name = "textBoxTele";
            this.textBoxTele.Size = new System.Drawing.Size(105, 22);
            this.textBoxTele.TabIndex = 12;
            // 
            // textBoxDyrektor
            // 
            this.textBoxDyrektor.Location = new System.Drawing.Point(170, 170);
            this.textBoxDyrektor.Name = "textBoxDyrektor";
            this.textBoxDyrektor.Size = new System.Drawing.Size(105, 22);
            this.textBoxDyrektor.TabIndex = 11;
            // 
            // textBoxRemont
            // 
            this.textBoxRemont.Location = new System.Drawing.Point(170, 132);
            this.textBoxRemont.Name = "textBoxRemont";
            this.textBoxRemont.Size = new System.Drawing.Size(105, 22);
            this.textBoxRemont.TabIndex = 10;
            // 
            // textBoxAdres
            // 
            this.textBoxAdres.Location = new System.Drawing.Point(170, 95);
            this.textBoxAdres.Name = "textBoxAdres";
            this.textBoxAdres.Size = new System.Drawing.Size(105, 22);
            this.textBoxAdres.TabIndex = 9;
            // 
            // textBoxNR
            // 
            this.textBoxNR.Location = new System.Drawing.Point(170, 60);
            this.textBoxNR.Name = "textBoxNR";
            this.textBoxNR.Size = new System.Drawing.Size(105, 22);
            this.textBoxNR.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 205);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "Telefon";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 173);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Dyrektor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Remont";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Adres";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "NR Odziału";
            // 
            // buttonDel
            // 
            this.buttonDel.Location = new System.Drawing.Point(231, 358);
            this.buttonDel.Name = "buttonDel";
            this.buttonDel.Size = new System.Drawing.Size(93, 90);
            this.buttonDel.TabIndex = 2;
            this.buttonDel.Text = "Delete";
            this.buttonDel.UseVisualStyleBackColor = true;
            this.buttonDel.Click += new System.EventHandler(this.buttonDel_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(67, 358);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(93, 90);
            this.buttonAdd.TabIndex = 1;
            this.buttonAdd.Text = "ADD";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // dataGridViewBank
            // 
            this.dataGridViewBank.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewBank.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBank.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.c,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8});
            this.dataGridViewBank.Location = new System.Drawing.Point(361, 14);
            this.dataGridViewBank.Name = "dataGridViewBank";
            this.dataGridViewBank.RowHeadersWidth = 51;
            this.dataGridViewBank.RowTemplate.Height = 24;
            this.dataGridViewBank.Size = new System.Drawing.Size(697, 459);
            this.dataGridViewBank.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "ID";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "ID Instalacji";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "ID Miejscowosci";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // c
            // 
            this.c.HeaderText = "Nr oddzialu";
            this.c.MinimumWidth = 6;
            this.c.Name = "c";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Adres";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Remont";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Dyrektor";
            this.Column7.MinimumWidth = 6;
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Telefon";
            this.Column8.MinimumWidth = 6;
            this.Column8.Name = "Column8";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1085, 485);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Pozostale";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridViewIns);
            this.panel2.Controls.Add(this.dataGridViewMiej);
            this.panel2.Controls.Add(this.button5);
            this.panel2.Controls.Add(this.buttonAddIns);
            this.panel2.Controls.Add(this.buttonDelMiej);
            this.panel2.Controls.Add(this.buttonAddMiej);
            this.panel2.Controls.Add(this.textBoxSD);
            this.panel2.Controls.Add(this.textBoxSS);
            this.panel2.Controls.Add(this.textBoxSwiatlo);
            this.panel2.Controls.Add(this.textBoxWoje);
            this.panel2.Controls.Add(this.textBoxMiejscowosc);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(0, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1063, 479);
            this.panel2.TabIndex = 0;
            // 
            // dataGridViewIns
            // 
            this.dataGridViewIns.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewIns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewIns.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.Column4});
            this.dataGridViewIns.Location = new System.Drawing.Point(379, 285);
            this.dataGridViewIns.Name = "dataGridViewIns";
            this.dataGridViewIns.RowHeadersWidth = 51;
            this.dataGridViewIns.RowTemplate.Height = 24;
            this.dataGridViewIns.Size = new System.Drawing.Size(495, 150);
            this.dataGridViewIns.TabIndex = 15;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Swiatlo";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "SSWIN";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "SDTV";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            // 
            // dataGridViewMiej
            // 
            this.dataGridViewMiej.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewMiej.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMiej.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dataGridViewMiej.Location = new System.Drawing.Point(379, 68);
            this.dataGridViewMiej.Name = "dataGridViewMiej";
            this.dataGridViewMiej.RowHeadersWidth = 51;
            this.dataGridViewMiej.RowTemplate.Height = 24;
            this.dataGridViewMiej.Size = new System.Drawing.Size(495, 150);
            this.dataGridViewMiej.TabIndex = 14;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Miejscowosc";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Wojewodztwo";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(217, 435);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(92, 38);
            this.button5.TabIndex = 13;
            this.button5.Text = "Delete";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // buttonAddIns
            // 
            this.buttonAddIns.Location = new System.Drawing.Point(71, 435);
            this.buttonAddIns.Name = "buttonAddIns";
            this.buttonAddIns.Size = new System.Drawing.Size(92, 38);
            this.buttonAddIns.TabIndex = 12;
            this.buttonAddIns.Text = "Add";
            this.buttonAddIns.UseVisualStyleBackColor = true;
            this.buttonAddIns.Click += new System.EventHandler(this.buttonAddIns_Click);
            // 
            // buttonDelMiej
            // 
            this.buttonDelMiej.Location = new System.Drawing.Point(217, 180);
            this.buttonDelMiej.Name = "buttonDelMiej";
            this.buttonDelMiej.Size = new System.Drawing.Size(92, 38);
            this.buttonDelMiej.TabIndex = 11;
            this.buttonDelMiej.Text = "Delete";
            this.buttonDelMiej.UseVisualStyleBackColor = true;
            this.buttonDelMiej.Click += new System.EventHandler(this.buttonDelMiej_Click);
            // 
            // buttonAddMiej
            // 
            this.buttonAddMiej.Location = new System.Drawing.Point(71, 180);
            this.buttonAddMiej.Name = "buttonAddMiej";
            this.buttonAddMiej.Size = new System.Drawing.Size(92, 38);
            this.buttonAddMiej.TabIndex = 10;
            this.buttonAddMiej.Text = "Add";
            this.buttonAddMiej.UseVisualStyleBackColor = true;
            this.buttonAddMiej.Click += new System.EventHandler(this.buttonAddMiej_Click);
            // 
            // textBoxSD
            // 
            this.textBoxSD.Location = new System.Drawing.Point(178, 379);
            this.textBoxSD.Name = "textBoxSD";
            this.textBoxSD.Size = new System.Drawing.Size(131, 22);
            this.textBoxSD.TabIndex = 9;
            // 
            // textBoxSS
            // 
            this.textBoxSS.Location = new System.Drawing.Point(178, 339);
            this.textBoxSS.Name = "textBoxSS";
            this.textBoxSS.Size = new System.Drawing.Size(131, 22);
            this.textBoxSS.TabIndex = 8;
            // 
            // textBoxSwiatlo
            // 
            this.textBoxSwiatlo.Location = new System.Drawing.Point(178, 296);
            this.textBoxSwiatlo.Name = "textBoxSwiatlo";
            this.textBoxSwiatlo.Size = new System.Drawing.Size(131, 22);
            this.textBoxSwiatlo.TabIndex = 7;
            // 
            // textBoxWoje
            // 
            this.textBoxWoje.Location = new System.Drawing.Point(178, 122);
            this.textBoxWoje.Name = "textBoxWoje";
            this.textBoxWoje.Size = new System.Drawing.Size(131, 22);
            this.textBoxWoje.TabIndex = 6;
            // 
            // textBoxMiejscowosc
            // 
            this.textBoxMiejscowosc.Location = new System.Drawing.Point(178, 79);
            this.textBoxMiejscowosc.Name = "textBoxMiejscowosc";
            this.textBoxMiejscowosc.Size = new System.Drawing.Size(131, 22);
            this.textBoxMiejscowosc.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(59, 382);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "SDTV";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(59, 339);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "SSWIN";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(59, 299);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Swiatlo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(59, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Wojewodztwo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(59, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Miescowosc";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1129, 538);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBank)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMiej)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxTele;
        private System.Windows.Forms.TextBox textBoxDyrektor;
        private System.Windows.Forms.TextBox textBoxRemont;
        private System.Windows.Forms.TextBox textBoxAdres;
        private System.Windows.Forms.TextBox textBoxNR;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonDel;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.DataGridView dataGridViewBank;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridViewIns;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridView dataGridViewMiej;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button buttonAddIns;
        private System.Windows.Forms.Button buttonDelMiej;
        private System.Windows.Forms.Button buttonAddMiej;
        private System.Windows.Forms.TextBox textBoxSD;
        private System.Windows.Forms.TextBox textBoxSS;
        private System.Windows.Forms.TextBox textBoxSwiatlo;
        private System.Windows.Forms.TextBox textBoxWoje;
        private System.Windows.Forms.TextBox textBoxMiejscowosc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn c;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
    }
}