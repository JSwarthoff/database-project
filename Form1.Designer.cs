﻿namespace BazyDanyc
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewMain = new System.Windows.Forms.DataGridView();
            this.zadanieDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.zadanieDataSet = new BazyDanyc.ZadanieDataSet();
            this.tabPageForm = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPageData = new System.Windows.Forms.TabPage();
            this.panelZlecenia = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewBank = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxOpis = new System.Windows.Forms.TextBox();
            this.buttonDelZlec = new System.Windows.Forms.Button();
            this.buttonUpdateZlec = new System.Windows.Forms.Button();
            this.buttonAddZle = new System.Windows.Forms.Button();
            this.buttonPokazZadania = new System.Windows.Forms.Button();
            this.panelPracownicy = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxImiePrac = new System.Windows.Forms.TextBox();
            this.textBoxAdresPrac = new System.Windows.Forms.TextBox();
            this.textBoxNazPrac = new System.Windows.Forms.TextBox();
            this.textBoxNrPrac = new System.Windows.Forms.TextBox();
            this.buttonDeletePracownik = new System.Windows.Forms.Button();
            this.buttonUpdatePracownik = new System.Windows.Forms.Button();
            this.buttonAddPracownik = new System.Windows.Forms.Button();
            this.buttonPokazPracownicy = new System.Windows.Forms.Button();
            this.bankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bankTableAdapter = new BazyDanyc.ZadanieDataSetTableAdapters.BankTableAdapter();
            this.miejscowosciBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.miejscowosciTableAdapter = new BazyDanyc.ZadanieDataSetTableAdapters.MiejscowosciTableAdapter();
            this.zleceniaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.zleceniaTableAdapter = new BazyDanyc.ZadanieDataSetTableAdapters.ZleceniaTableAdapter();
            this.bankBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_Banku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox2 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox3 = new System.Windows.Forms.MaskedTextBox();
            this.dataGridViewZlec = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zadanieDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zadanieDataSet)).BeginInit();
            this.tabPageForm.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPageData.SuspendLayout();
            this.panelZlecenia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBank)).BeginInit();
            this.panelPracownicy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.miejscowosciBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zleceniaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewZlec)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewMain
            // 
            this.dataGridViewMain.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Column1,
            this.ID_Banku,
            this.Column2,
            this.Column3});
            this.dataGridViewMain.Location = new System.Drawing.Point(8, 6);
            this.dataGridViewMain.MultiSelect = false;
            this.dataGridViewMain.Name = "dataGridViewMain";
            this.dataGridViewMain.RowHeadersWidth = 51;
            this.dataGridViewMain.RowTemplate.Height = 24;
            this.dataGridViewMain.Size = new System.Drawing.Size(1034, 282);
            this.dataGridViewMain.TabIndex = 0;
            this.dataGridViewMain.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMain_CellContentClick);
            // 
            // zadanieDataSetBindingSource
            // 
            this.zadanieDataSetBindingSource.DataSource = this.zadanieDataSet;
            this.zadanieDataSetBindingSource.Position = 0;
            // 
            // zadanieDataSet
            // 
            this.zadanieDataSet.DataSetName = "ZadanieDataSet";
            this.zadanieDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPageForm
            // 
            this.tabPageForm.Controls.Add(this.tabPage2);
            this.tabPageForm.Controls.Add(this.tabPageData);
            this.tabPageForm.Location = new System.Drawing.Point(0, 0);
            this.tabPageForm.Name = "tabPageForm";
            this.tabPageForm.SelectedIndex = 0;
            this.tabPageForm.Size = new System.Drawing.Size(1056, 520);
            this.tabPageForm.TabIndex = 9;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridViewZlec);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1048, 491);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Form";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPageData
            // 
            this.tabPageData.Controls.Add(this.panelZlecenia);
            this.tabPageData.Controls.Add(this.buttonPokazZadania);
            this.tabPageData.Controls.Add(this.panelPracownicy);
            this.tabPageData.Controls.Add(this.buttonPokazPracownicy);
            this.tabPageData.Controls.Add(this.dataGridViewMain);
            this.tabPageData.Location = new System.Drawing.Point(4, 25);
            this.tabPageData.Name = "tabPageData";
            this.tabPageData.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageData.Size = new System.Drawing.Size(1048, 491);
            this.tabPageData.TabIndex = 0;
            this.tabPageData.Text = "Data";
            this.tabPageData.UseVisualStyleBackColor = true;
            // 
            // panelZlecenia
            // 
            this.panelZlecenia.Controls.Add(this.maskedTextBox3);
            this.panelZlecenia.Controls.Add(this.maskedTextBox2);
            this.panelZlecenia.Controls.Add(this.maskedTextBox1);
            this.panelZlecenia.Controls.Add(this.label1);
            this.panelZlecenia.Controls.Add(this.dataGridViewBank);
            this.panelZlecenia.Controls.Add(this.label11);
            this.panelZlecenia.Controls.Add(this.label12);
            this.panelZlecenia.Controls.Add(this.label13);
            this.panelZlecenia.Controls.Add(this.textBoxOpis);
            this.panelZlecenia.Controls.Add(this.buttonDelZlec);
            this.panelZlecenia.Controls.Add(this.buttonUpdateZlec);
            this.panelZlecenia.Controls.Add(this.buttonAddZle);
            this.panelZlecenia.Location = new System.Drawing.Point(6, 323);
            this.panelZlecenia.Name = "panelZlecenia";
            this.panelZlecenia.Size = new System.Drawing.Size(1022, 198);
            this.panelZlecenia.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(134, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Zakonczenie";
            // 
            // dataGridViewBank
            // 
            this.dataGridViewBank.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBank.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8});
            this.dataGridViewBank.Location = new System.Drawing.Point(443, 0);
            this.dataGridViewBank.Name = "dataGridViewBank";
            this.dataGridViewBank.RowHeadersWidth = 51;
            this.dataGridViewBank.RowTemplate.Height = 24;
            this.dataGridViewBank.Size = new System.Drawing.Size(576, 136);
            this.dataGridViewBank.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(161, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 17);
            this.label11.TabIndex = 12;
            this.label11.Text = "Status";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(134, 41);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 17);
            this.label12.TabIndex = 11;
            this.label12.Text = "Rozpoczecie";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(186, 111);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 17);
            this.label13.TabIndex = 10;
            this.label13.Text = "Opis";
            // 
            // textBoxOpis
            // 
            this.textBoxOpis.Location = new System.Drawing.Point(254, 108);
            this.textBoxOpis.Multiline = true;
            this.textBoxOpis.Name = "textBoxOpis";
            this.textBoxOpis.Size = new System.Drawing.Size(156, 42);
            this.textBoxOpis.TabIndex = 4;
            // 
            // buttonDelZlec
            // 
            this.buttonDelZlec.Location = new System.Drawing.Point(9, 73);
            this.buttonDelZlec.Name = "buttonDelZlec";
            this.buttonDelZlec.Size = new System.Drawing.Size(75, 23);
            this.buttonDelZlec.TabIndex = 2;
            this.buttonDelZlec.Text = "Delete";
            this.buttonDelZlec.UseVisualStyleBackColor = true;
            this.buttonDelZlec.Click += new System.EventHandler(this.buttonDelZlec_Click);
            // 
            // buttonUpdateZlec
            // 
            this.buttonUpdateZlec.Location = new System.Drawing.Point(9, 44);
            this.buttonUpdateZlec.Name = "buttonUpdateZlec";
            this.buttonUpdateZlec.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdateZlec.TabIndex = 1;
            this.buttonUpdateZlec.Text = "Update";
            this.buttonUpdateZlec.UseVisualStyleBackColor = true;
            this.buttonUpdateZlec.Click += new System.EventHandler(this.buttonUpdateZlec_Click);
            // 
            // buttonAddZle
            // 
            this.buttonAddZle.Location = new System.Drawing.Point(9, 12);
            this.buttonAddZle.Name = "buttonAddZle";
            this.buttonAddZle.Size = new System.Drawing.Size(75, 23);
            this.buttonAddZle.TabIndex = 0;
            this.buttonAddZle.Text = "Add";
            this.buttonAddZle.UseVisualStyleBackColor = true;
            this.buttonAddZle.Click += new System.EventHandler(this.buttonAddZle_Click);
            // 
            // buttonPokazZadania
            // 
            this.buttonPokazZadania.Location = new System.Drawing.Point(170, 294);
            this.buttonPokazZadania.Name = "buttonPokazZadania";
            this.buttonPokazZadania.Size = new System.Drawing.Size(165, 23);
            this.buttonPokazZadania.TabIndex = 2;
            this.buttonPokazZadania.Text = "Zlecenia";
            this.buttonPokazZadania.UseVisualStyleBackColor = true;
            this.buttonPokazZadania.Click += new System.EventHandler(this.buttonPokazZadania_Click);
            // 
            // panelPracownicy
            // 
            this.panelPracownicy.Controls.Add(this.label10);
            this.panelPracownicy.Controls.Add(this.label9);
            this.panelPracownicy.Controls.Add(this.label8);
            this.panelPracownicy.Controls.Add(this.label7);
            this.panelPracownicy.Controls.Add(this.textBoxImiePrac);
            this.panelPracownicy.Controls.Add(this.textBoxAdresPrac);
            this.panelPracownicy.Controls.Add(this.textBoxNazPrac);
            this.panelPracownicy.Controls.Add(this.textBoxNrPrac);
            this.panelPracownicy.Controls.Add(this.buttonDeletePracownik);
            this.panelPracownicy.Controls.Add(this.buttonUpdatePracownik);
            this.panelPracownicy.Controls.Add(this.buttonAddPracownik);
            this.panelPracownicy.Location = new System.Drawing.Point(8, 323);
            this.panelPracownicy.Name = "panelPracownicy";
            this.panelPracownicy.Size = new System.Drawing.Size(1019, 167);
            this.panelPracownicy.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(217, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 17);
            this.label10.TabIndex = 13;
            this.label10.Text = "Adres";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(225, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 17);
            this.label9.TabIndex = 12;
            this.label9.Text = "Imię";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(204, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 17);
            this.label8.TabIndex = 11;
            this.label8.Text = "Nazwisko";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(166, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 17);
            this.label7.TabIndex = 10;
            this.label7.Text = "Numer telefonu";
            // 
            // textBoxImiePrac
            // 
            this.textBoxImiePrac.Location = new System.Drawing.Point(286, 13);
            this.textBoxImiePrac.Name = "textBoxImiePrac";
            this.textBoxImiePrac.Size = new System.Drawing.Size(100, 22);
            this.textBoxImiePrac.TabIndex = 7;
            // 
            // textBoxAdresPrac
            // 
            this.textBoxAdresPrac.Location = new System.Drawing.Point(286, 69);
            this.textBoxAdresPrac.Name = "textBoxAdresPrac";
            this.textBoxAdresPrac.Size = new System.Drawing.Size(100, 22);
            this.textBoxAdresPrac.TabIndex = 6;
            // 
            // textBoxNazPrac
            // 
            this.textBoxNazPrac.Location = new System.Drawing.Point(286, 41);
            this.textBoxNazPrac.Name = "textBoxNazPrac";
            this.textBoxNazPrac.Size = new System.Drawing.Size(100, 22);
            this.textBoxNazPrac.TabIndex = 5;
            // 
            // textBoxNrPrac
            // 
            this.textBoxNrPrac.Location = new System.Drawing.Point(286, 97);
            this.textBoxNrPrac.Name = "textBoxNrPrac";
            this.textBoxNrPrac.Size = new System.Drawing.Size(100, 22);
            this.textBoxNrPrac.TabIndex = 4;
            // 
            // buttonDeletePracownik
            // 
            this.buttonDeletePracownik.Location = new System.Drawing.Point(9, 73);
            this.buttonDeletePracownik.Name = "buttonDeletePracownik";
            this.buttonDeletePracownik.Size = new System.Drawing.Size(75, 23);
            this.buttonDeletePracownik.TabIndex = 2;
            this.buttonDeletePracownik.Text = "Delete";
            this.buttonDeletePracownik.UseVisualStyleBackColor = true;
            this.buttonDeletePracownik.Click += new System.EventHandler(this.buttonDeletePracownik_Click);
            // 
            // buttonUpdatePracownik
            // 
            this.buttonUpdatePracownik.Location = new System.Drawing.Point(9, 44);
            this.buttonUpdatePracownik.Name = "buttonUpdatePracownik";
            this.buttonUpdatePracownik.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdatePracownik.TabIndex = 1;
            this.buttonUpdatePracownik.Text = "Update";
            this.buttonUpdatePracownik.UseVisualStyleBackColor = true;
            this.buttonUpdatePracownik.Click += new System.EventHandler(this.buttonUpdatePracownik_Click);
            // 
            // buttonAddPracownik
            // 
            this.buttonAddPracownik.Location = new System.Drawing.Point(9, 12);
            this.buttonAddPracownik.Name = "buttonAddPracownik";
            this.buttonAddPracownik.Size = new System.Drawing.Size(75, 23);
            this.buttonAddPracownik.TabIndex = 0;
            this.buttonAddPracownik.Text = "Add";
            this.buttonAddPracownik.UseVisualStyleBackColor = true;
            this.buttonAddPracownik.Click += new System.EventHandler(this.buttonAddPracownik_Click);
            // 
            // buttonPokazPracownicy
            // 
            this.buttonPokazPracownicy.Location = new System.Drawing.Point(8, 294);
            this.buttonPokazPracownicy.Name = "buttonPokazPracownicy";
            this.buttonPokazPracownicy.Size = new System.Drawing.Size(156, 23);
            this.buttonPokazPracownicy.TabIndex = 1;
            this.buttonPokazPracownicy.Text = "Pracownicy";
            this.buttonPokazPracownicy.UseVisualStyleBackColor = true;
            this.buttonPokazPracownicy.Click += new System.EventHandler(this.buttonPokazPracownicy_Click);
            // 
            // bankBindingSource
            // 
            this.bankBindingSource.DataMember = "Bank";
            this.bankBindingSource.DataSource = this.zadanieDataSetBindingSource;
            // 
            // bankTableAdapter
            // 
            this.bankTableAdapter.ClearBeforeFill = true;
            // 
            // miejscowosciBindingSource
            // 
            this.miejscowosciBindingSource.DataMember = "Miejscowosci";
            this.miejscowosciBindingSource.DataSource = this.zadanieDataSetBindingSource;
            // 
            // miejscowosciTableAdapter
            // 
            this.miejscowosciTableAdapter.ClearBeforeFill = true;
            // 
            // zleceniaBindingSource
            // 
            this.zleceniaBindingSource.DataMember = "Zlecenia";
            this.zleceniaBindingSource.DataSource = this.zadanieDataSetBindingSource;
            // 
            // zleceniaTableAdapter
            // 
            this.zleceniaTableAdapter.ClearBeforeFill = true;
            // 
            // bankBindingSource1
            // 
            this.bankBindingSource1.DataMember = "Bank";
            this.bankBindingSource1.DataSource = this.zadanieDataSetBindingSource;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 6;
            this.ID.Name = "ID";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Imie";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            // 
            // ID_Banku
            // 
            this.ID_Banku.HeaderText = "Nazwisko";
            this.ID_Banku.MinimumWidth = 6;
            this.ID_Banku.Name = "ID_Banku";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Adres";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Nr Telefonu";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 65;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "ID_Instalacji";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 66;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "ID_Miejscowosci";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 65;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Nr Odzialu";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.Width = 66;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Adres";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.Width = 65;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Remont";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            this.Column6.Width = 65;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Dyrektor";
            this.Column7.MinimumWidth = 6;
            this.Column7.Name = "Column7";
            this.Column7.Width = 66;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Telefon";
            this.Column8.MinimumWidth = 6;
            this.Column8.Name = "Column8";
            this.Column8.Width = 65;
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(254, 10);
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(100, 22);
            this.maskedTextBox1.TabIndex = 15;
            // 
            // maskedTextBox2
            // 
            this.maskedTextBox2.Location = new System.Drawing.Point(254, 41);
            this.maskedTextBox2.Mask = "00/00/0000";
            this.maskedTextBox2.Name = "maskedTextBox2";
            this.maskedTextBox2.Size = new System.Drawing.Size(100, 22);
            this.maskedTextBox2.TabIndex = 16;
            this.maskedTextBox2.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox3
            // 
            this.maskedTextBox3.Location = new System.Drawing.Point(254, 72);
            this.maskedTextBox3.Mask = "00/00/0000";
            this.maskedTextBox3.Name = "maskedTextBox3";
            this.maskedTextBox3.Size = new System.Drawing.Size(100, 22);
            this.maskedTextBox3.TabIndex = 17;
            this.maskedTextBox3.ValidatingType = typeof(System.DateTime);
            // 
            // dataGridViewZlec
            // 
            this.dataGridViewZlec.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewZlec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewZlec.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.Column9,
            this.Column10,
            this.Column11});
            this.dataGridViewZlec.Location = new System.Drawing.Point(47, 33);
            this.dataGridViewZlec.Name = "dataGridViewZlec";
            this.dataGridViewZlec.RowHeadersWidth = 51;
            this.dataGridViewZlec.RowTemplate.Height = 24;
            this.dataGridViewZlec.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewZlec.Size = new System.Drawing.Size(968, 400);
            this.dataGridViewZlec.TabIndex = 0;
            this.dataGridViewZlec.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewZlec_CellContentClick);
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "ID";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "ID_Pracownika";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "ID_Banku";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Opis";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Status";
            this.Column9.MinimumWidth = 6;
            this.Column9.Name = "Column9";
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Data Rozpoczecia";
            this.Column10.MinimumWidth = 6;
            this.Column10.Name = "Column10";
            // 
            // Column11
            // 
            this.Column11.HeaderText = "Data Zakonczenia";
            this.Column11.MinimumWidth = 6;
            this.Column11.Name = "Column11";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1068, 527);
            this.Controls.Add(this.tabPageForm);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zadanieDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zadanieDataSet)).EndInit();
            this.tabPageForm.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPageData.ResumeLayout(false);
            this.panelZlecenia.ResumeLayout(false);
            this.panelZlecenia.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBank)).EndInit();
            this.panelPracownicy.ResumeLayout(false);
            this.panelPracownicy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.miejscowosciBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zleceniaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewZlec)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewMain;
        private System.Windows.Forms.BindingSource zadanieDataSetBindingSource;
        private ZadanieDataSet zadanieDataSet;
        private System.Windows.Forms.TabControl tabPageForm;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPageData;
        private System.Windows.Forms.BindingSource bankBindingSource;
        private ZadanieDataSetTableAdapters.BankTableAdapter bankTableAdapter;
        private System.Windows.Forms.BindingSource miejscowosciBindingSource;
        private ZadanieDataSetTableAdapters.MiejscowosciTableAdapter miejscowosciTableAdapter;
        private System.Windows.Forms.BindingSource zleceniaBindingSource;
        private ZadanieDataSetTableAdapters.ZleceniaTableAdapter zleceniaTableAdapter;
        private System.Windows.Forms.Panel panelZlecenia;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxOpis;
        private System.Windows.Forms.Button buttonDelZlec;
        private System.Windows.Forms.Button buttonUpdateZlec;
        private System.Windows.Forms.Button buttonAddZle;
        private System.Windows.Forms.Panel panelPracownicy;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxImiePrac;
        private System.Windows.Forms.TextBox textBoxAdresPrac;
        private System.Windows.Forms.TextBox textBoxNazPrac;
        private System.Windows.Forms.TextBox textBoxNrPrac;
        private System.Windows.Forms.Button buttonDeletePracownik;
        private System.Windows.Forms.Button buttonUpdatePracownik;
        private System.Windows.Forms.Button buttonAddPracownik;
        private System.Windows.Forms.Button buttonPokazZadania;
        private System.Windows.Forms.Button buttonPokazPracownicy;
        private System.Windows.Forms.BindingSource bankBindingSource1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewBank;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Banku;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.MaskedTextBox maskedTextBox3;
        private System.Windows.Forms.MaskedTextBox maskedTextBox2;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.DataGridView dataGridViewZlec;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
    }
}

