﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BazyDanyc
{
    public partial class Form2 : Form
    {
        static ZadanieEntities dbContext;
        public Form2()
        {
            InitializeComponent();
            dbContext = new ZadanieEntities();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            List<Instalacje> ins = new List<Instalacje>();
            ins = dbContext.Instalacje.ToList();
            List<Miejscowosci> miej = new List<Miejscowosci>();
            miej = dbContext.Miejscowosci.ToList();
            List<Bank> bank = new List<Bank>();
            bank = dbContext.Bank.ToList();
            foreach (Instalacje z in ins)
            {
                string[] sss = { z.ID.ToString(), z.Swiatlo.ToString(), z.SSWIN.ToString(), z.SDTV.ToString() };
                dataGridViewIns.Rows.Add(sss);
            }
            foreach (Miejscowosci z in miej)
            {
                string[] sss = { z.ID.ToString(), z.Miejscowosc, z.Wojewodztwo};
                dataGridViewMiej.Rows.Add(sss);
            }
            foreach (Bank z in bank)
            {
                string[] sss = { z.ID.ToString(), z.ID_instalacje, z.ID_miejscowosci.ToString(), z.nr_oddzialu.ToString(), z.adres, z.dyrektor, z.telefon.ToString() };
                dataGridViewBank.Rows.Add(sss);
            }
        }
        private void refreshBank()
        {
            dataGridViewBank.Rows.Clear();
            List<Bank> prac = new List<Bank>();
            prac = dbContext.Bank.ToList();
            foreach (Bank z in prac)
            {
                string[] sss = { z.ID.ToString(), z.ID_instalacje, z.ID_miejscowosci.ToString(), z.nr_oddzialu.ToString(), z.adres, z.dyrektor, z.telefon.ToString() };
                dataGridViewBank.Rows.Add(sss);
            }
        }
        private void refreshMiej()
        {
            dataGridViewMiej.Rows.Clear();
            List<Miejscowosci> prac = new List<Miejscowosci>();
            prac = dbContext.Miejscowosci.ToList();
            foreach (Miejscowosci z in prac)
            {
                string[] sss = { z.ID.ToString(), z.Miejscowosc, z.Wojewodztwo};
                dataGridViewMiej.Rows.Add(sss);
            }
        }
        private void refreshIns()
        {
            dataGridViewIns.Rows.Clear();
            List<Instalacje> prac = new List<Instalacje>();
            prac = dbContext.Instalacje.ToList();
            foreach (Instalacje z in prac)
            {
                string[] sss = { z.ID.ToString(), z.Swiatlo.ToString(), z.SSWIN.ToString(), z.SDTV.ToString() };
                dataGridViewBank.Rows.Add(sss);
            }
        }
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (dataGridViewMiej.SelectedRows.Count == 0)
            {
                return;
            }
            if (dataGridViewIns.SelectedRows.Count == 0)
            {
                return;
            }
            int ins = Int32.Parse(dataGridViewIns.SelectedRows[0].Cells[0].Value.ToString().Trim());
            int miej = Int32.Parse(dataGridViewMiej.SelectedRows[0].Cells[0].Value.ToString().Trim());
            Bank prac = new Bank();
            List<Bank> zlec = new List<Bank>();
            zlec = dbContext.Bank.ToList();
            prac.ID = zlec.Last().ID + 1;
            prac.ID_instalacje = ins.ToString();
            prac.ID_miejscowosci = miej.ToString();
            prac.nr_oddzialu = Int32.Parse(textBoxNR.Text);
            prac.adres = textBoxAdres.Text;
            prac.dyrektor = textBoxDyrektor.Text;
            prac.telefon = textBoxTele.Text;
            dbContext.Bank.Add(prac);
            dbContext.SaveChanges();
            textBoxNR.Text = ""; textBoxAdres.Text=""; textBoxDyrektor.Text=""; textBoxTele.Text="";
            refreshBank();
        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            if (dataGridViewBank.SelectedRows.Count == 0)
            {
                return;
            }
            int zlec = Int32.Parse(dataGridViewBank.SelectedRows[0].Cells[0].Value.ToString().Trim());
            var result = dbContext.Bank.SingleOrDefault(b => b.ID == zlec);
            if (result != null)
            {
                try
                {
                    dbContext.Bank.Remove(result);
                    dbContext.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
            refreshBank();
        }

        private void buttonAddMiej_Click(object sender, EventArgs e)
        {
            Miejscowosci prac = new Miejscowosci();
            List<Miejscowosci> zlec = new List<Miejscowosci>();
            zlec = dbContext.Miejscowosci.ToList();
            int a = Int32.Parse(zlec.Last().ID) + 1;
            prac.ID = a.ToString();
            prac.Miejscowosc = textBoxMiejscowosc.Text;
            prac.Wojewodztwo = textBoxWoje.Text;
            dbContext.Miejscowosci.Add(prac);
            dbContext.SaveChanges();
            textBoxMiejscowosc.Text = ""; textBoxWoje.Text = ""; 
            refreshMiej();
        }

        private void buttonDelMiej_Click(object sender, EventArgs e)
        {
            if (dataGridViewMiej.SelectedRows.Count == 0)
            {
                return;
            }
            string zlec = dataGridViewMiej.SelectedRows[0].Cells[0].Value.ToString().Trim();
            var result = dbContext.Miejscowosci.SingleOrDefault(b => b.ID == zlec);
            if (result != null)
            {
                try
                {
                    dbContext.Miejscowosci.Remove(result);
                    dbContext.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
            refreshMiej();
        }

        private void buttonAddIns_Click(object sender, EventArgs e)
        {
            Instalacje prac = new Instalacje();
            List<Instalacje> zlec = new List<Instalacje>();
            zlec = dbContext.Instalacje.ToList();
            int a = Int32.Parse(zlec.Last().ID) + 1;
            prac.ID = a.ToString();
            if (textBoxSwiatlo.Text == "")
                prac.Swiatlo = true;
            else
                prac.Swiatlo = false;
            if (textBoxSS.Text == "")
                prac.SSWIN = true;
            else
                prac.SSWIN = false;
            if (textBoxSD.Text == "")
                prac.SDTV = true;
            else
                prac.SDTV = false;
            dbContext.Instalacje.Add(prac);
            dbContext.SaveChanges();
            textBoxSwiatlo.Text = ""; textBoxSS.Text = ""; textBoxSD.Text = "";
            refreshIns();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (dataGridViewIns.SelectedRows.Count == 0)
            {
                return;
            }
            string zlec = dataGridViewIns.SelectedRows[0].Cells[0].Value.ToString().Trim();
            var result = dbContext.Instalacje.SingleOrDefault(b => b.ID == zlec);
            if (result != null)
            {
                try
                {
                    dbContext.Instalacje.Remove(result);
                    dbContext.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
            refreshIns();
        }

        
    }
}
