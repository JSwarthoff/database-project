﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BazyDanyc
{
    public partial class Form1 : Form
    {

        static ZadanieEntities dbContext;
        
        public Form1()
        {
            dbContext = new ZadanieEntities();
            InitializeComponent();
        }
       
        private void Form1_Load(object sender, EventArgs e)
        {
            //Widocznosc paneli
            Form2 s = new Form2();
            s.ShowDialog();
            panelZlecenia.Visible = false;
            panelPracownicy.Visible = true;
            List<Zlecenia> zlec = new List<Zlecenia>();
            zlec = dbContext.Zlecenia.ToList();
            List<Pracownik> prac = new List<Pracownik>();
            prac = dbContext.Pracownik.ToList();
            List<Bank> bank = new List<Bank>();
            bank = dbContext.Bank.ToList();
            foreach (Zlecenia z in zlec)
            {
                string[] sss = { z.ID.ToString(), z.ID_Banku.ToString(), z.ID_pracownika.ToString(), z.Opis, z.Status.ToString(), z.DataRozpoczecia.ToString(), z.DataZakonczenia.ToString() };
                dataGridViewZlec.Rows.Add(sss);
            }
            foreach (Pracownik z in prac)
            {
                string[] sss = { z.ID.ToString(), z.Imie, z.Nazwisko.ToString(), z.Adres, z.Nrtelefonu };
                dataGridViewMain.Rows.Add(sss);
            }
            foreach (Bank z in bank)
            {
                string[] sss = { z.ID.ToString(), z.ID_instalacje, z.ID_miejscowosci.ToString(), z.nr_oddzialu.ToString(), z.adres,z.dyrektor,z.telefon.ToString() };
                dataGridViewBank.Rows.Add(sss);
            }
            //dataGridViewMain.DataSource = dbContext.Zlecenia.ToList();
            //dataGridViewBank.DataSource = dbContext.Bank.ToList();

        }

        private void refreshPrac()
        {
            dataGridViewMain.Rows.Clear();
            List<Pracownik> prac = new List<Pracownik>();
            prac = dbContext.Pracownik.ToList();
            foreach (Pracownik z in prac)
            {
                string[] sss = { z.ID.ToString(), z.Imie, z.Nazwisko.ToString(), z.Adres, z.Nrtelefonu };
                dataGridViewMain.Rows.Add(sss);
            }
        }
        private void refreshBank()
        {
            dataGridViewBank.Rows.Clear();
            List<Bank> prac = new List<Bank>();
            prac = dbContext.Bank.ToList();
            foreach (Bank z in prac)
            {
                string[] sss = { z.ID.ToString(), z.ID_instalacje, z.ID_miejscowosci.ToString(), z.nr_oddzialu.ToString(), z.adres, z.dyrektor, z.telefon.ToString() };
                dataGridViewBank.Rows.Add(sss);
            }
        }
        private void refreshZlec()
        {
            dataGridViewZlec.Rows.Clear();
            List<Zlecenia> prac = new List<Zlecenia>();
            prac = dbContext.Zlecenia.ToList();
            foreach (Zlecenia z in prac)
            {
                string[] sss = { z.ID.ToString(), z.ID_Banku.ToString(), z.ID_pracownika.ToString(), z.Opis, z.Status.ToString(), z.DataRozpoczecia.ToString(), z.DataZakonczenia.ToString() };
                dataGridViewZlec.Rows.Add(sss);
            }
        }

        private void buttonPokazPracownicy_Click(object sender, EventArgs e)
        {
            panelPracownicy.Visible = true;
            panelZlecenia.Visible = false;
        }

        private void buttonPokazZadania_Click(object sender, EventArgs e)
        {
            panelPracownicy.Visible = false;
            panelZlecenia.Visible = true;
        }

        private void dataGridViewMain_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int i = e.RowIndex;
            DataGridViewRow row =dataGridViewMain.Rows[i];
            textBoxImiePrac.Text = row.Cells[1].Value.ToString(); textBoxNazPrac.Text = row.Cells[2].Value.ToString(); textBoxAdresPrac.Text = row.Cells[3].Value.ToString(); textBoxNrPrac.Text = row.Cells[4].Value.ToString();
        }

        private void buttonAddPracownik_Click(object sender, EventArgs e)
        {
            Pracownik prac = new Pracownik();
            List<Pracownik> zlec = new List<Pracownik>();
            zlec = dbContext.Pracownik.ToList();
            prac.ID = zlec.Last().ID + 1;
            prac.Imie = textBoxImiePrac.Text;
            prac.Nazwisko = textBoxNazPrac.Text;
            prac.Adres = textBoxAdresPrac.Text;
            prac.Nrtelefonu = textBoxNrPrac.Text;
            dbContext.Pracownik.Add(prac);
            dbContext.SaveChanges();
            textBoxImiePrac.Text = ""; textBoxNazPrac.Text=""; textBoxAdresPrac.Text=""; textBoxNrPrac.Text="";
            refreshPrac();
            //sprawdz czy nie trzeba jakiegos update zrobic
        }

        private void buttonUpdatePracownik_Click(object sender, EventArgs e)
        {
            if (dataGridViewMain.SelectedRows.Count == 0)
            {
                return;
            }
            Pracownik prac =(Pracownik) dataGridViewMain.SelectedRows[0].DataBoundItem;
            int tag = Int32.Parse(dataGridViewMain.SelectedRows[0].Cells[0].Value.ToString().Trim());
            var result= dbContext.Pracownik.SingleOrDefault(b => b.ID ==tag);
            if (result != null)
            {
                try
                {
                    result.Imie = textBoxImiePrac.Text.Trim();
                    result.Nazwisko = textBoxNazPrac.Text.Trim();
                    result.Adres = textBoxAdresPrac.Text.Trim();
                    result.Nrtelefonu = textBoxNrPrac.Text.Trim();
                    dbContext.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
            refreshPrac();
        }

        private void buttonDeletePracownik_Click(object sender, EventArgs e)
        {
            if (dataGridViewMain.SelectedRows.Count == 0)
            {
                return;
            }
            int tag = Int32.Parse(dataGridViewMain.SelectedRows[0].Cells[0].Value.ToString().Trim());
            var result = dbContext.Pracownik.SingleOrDefault(b => b.ID == tag);
            if (result != null)
            {
                try
                {
                    dbContext.Pracownik.Remove(result);
                    dbContext.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
            refreshPrac();
        }

        private void buttonAddZle_Click(object sender, EventArgs e)
        {
            if (dataGridViewMain.SelectedRows.Count == 0)
            {
                return;
            }
            if (dataGridViewBank.SelectedRows.Count == 0)
            {
                return;
            }
            int prac = Int32.Parse(dataGridViewMain.SelectedRows[0].Cells[0].Value.ToString().Trim());
            int bank = Int32.Parse(dataGridViewBank.SelectedRows[0].Cells[0].Value.ToString().Trim());
            Zlecenia zlec = new Zlecenia();
            zlec.ID_Banku = bank;
            zlec.ID_pracownika = prac;
            zlec.Opis = textBoxOpis.Text;
            if (maskedTextBox1.Text == "")
                zlec.Status = true;
            else
                zlec.Status = false;
            
            zlec.DataRozpoczecia = DateTime.Parse(maskedTextBox2.Text);
            zlec.DataZakonczenia = null;
            dbContext.Zlecenia.Add(zlec);
            dbContext.SaveChanges();
            maskedTextBox1.Text = ""; maskedTextBox2.Text = "";textBoxOpis.Text = "";
            refreshZlec();
        }

        private void buttonUpdateZlec_Click(object sender, EventArgs e)
        {
            if (dataGridViewZlec.SelectedRows.Count == 0)
            {
                return;
            }
            int zlec = Int32.Parse(dataGridViewZlec.SelectedRows[0].Cells[0].Value.ToString().Trim());
            int prac = 0;
            int bank = 0;
            var result = dbContext.Zlecenia.SingleOrDefault(b => b.ID == zlec);
            if (result != null)
            {
                
                    if (dataGridViewMain.SelectedRows.Count != 0)
                    {
                        prac = Int32.Parse(dataGridViewMain.SelectedRows[0].Cells[0].Value.ToString().Trim());
                        result.ID_pracownika = prac;
                    }
                    if(dataGridViewBank.SelectedRows.Count != 0)
                    {
                        bank = Int32.Parse(dataGridViewBank.SelectedRows[0].Cells[0].Value.ToString().Trim());
                        result.ID_Banku = bank;
                    }
                    result.Opis = textBoxOpis.Text.Trim();
                    if (maskedTextBox1.Text == "")
                        result.Status = true;
                    else
                        result.Status = false;
                    result.DataRozpoczecia = DateTime.Parse(maskedTextBox2.Text);
                    result.DataZakonczenia = DateTime.Parse(maskedTextBox3.Text);
                    dbContext.SaveChanges();
             
            }
            refreshZlec();
        }

        private void buttonDelZlec_Click(object sender, EventArgs e)
        {
            if (dataGridViewZlec.SelectedRows.Count == 0)
            {
                return;
            }
            int zlec = Int32.Parse(dataGridViewZlec.SelectedRows[0].Cells[0].Value.ToString().Trim());
            var result = dbContext.Zlecenia.SingleOrDefault(b => b.ID == zlec);
            if (result != null)
            {
                try
                {
                    dbContext.Zlecenia.Remove(result);
                    dbContext.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
            refreshZlec();
        }

        private void dataGridViewZlec_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int i = e.RowIndex;
            DataGridViewRow row = dataGridViewZlec.Rows[i];
            maskedTextBox1.Text = row.Cells[4].Value.ToString();
            maskedTextBox2.Text = row.Cells[5].Value.ToString();
            maskedTextBox3.Text = row.Cells[6].Value.ToString();textBoxOpis.Text = row.Cells[3].Value.ToString();
        }
    }
}
